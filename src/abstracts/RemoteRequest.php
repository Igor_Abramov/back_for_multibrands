<?php


namespace Src\abstracts;


interface RemoteRequest
{
	public function sendRequest();
}
