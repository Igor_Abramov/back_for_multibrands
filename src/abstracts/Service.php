<?php


namespace Src\abstracts;


interface Service
{
	public function setParams(): void;

	public function execute(): void;

}
