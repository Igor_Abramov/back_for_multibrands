<?php


namespace Src\controllers;


use Symfony\Component\HttpFoundation\Response;

class TestController extends Controller
{

	public function test(): Response
	{
		return new Response('Test is done', 200);
	}

}
