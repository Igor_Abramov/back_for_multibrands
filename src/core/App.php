<?php


namespace Src\core;


use Src\core\Router;
use Symfony\Component\HttpFoundation\Request;

class App
{
	public function __construct()
	{
		$request = Request::createFromGlobals();
		new Router($request, APP_DIR);
	}
}
