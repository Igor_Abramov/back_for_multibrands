<?php


namespace Src\core;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing;

class Router
{
	public function __construct(Request $request, string $app_dir)
	{

		$fileLocator = new FileLocator([$app_dir]);
		$loader = new Routing\Loader\YamlFileLoader($fileLocator);
		$routes = $loader->load($app_dir . 'config/routes.yaml');

		$context = (new Routing\RequestContext())->fromRequest($request);
		$matcher = new Routing\Matcher\UrlMatcher($routes, $context);

		try{
			$attributes = $matcher->match($request->getPathInfo());
			$this->callController($request, $attributes);
		} catch (Routing\Exception\ResourceNotFoundException $exception) {
			(new Response('Not Found', 404))->send();
		} catch (\Exception $exception) {
			(new Response('Error', 500))->send();
		}
	}

	/**
	 * @param Request $request
	 * @param $attributes
	 */
	protected function callController(Request $request, $attributes): void
	{
		if(!empty($attributes['_controller']) && substr_count($attributes['_controller'], '::') === 1){
			$code = explode('::', $attributes['_controller']);
			$controller = (string)$code[0];
			$action = (string)$code[1];
			(new $controller($request, $attributes))->$action();
		}
	}





}
