<?php


namespace controllers;


use PHPUnit\Framework\TestCase;
use Src\controllers\TestController;

class TestControllerTest extends TestCase
{
	/**
	 * @var TestController
	 */
	private TestController $instance;


	public function setUp(): void
	{
		parent::setUp();
		$this->instance = new TestController();
	}

	public function testIsClassExist(): void
	{
		self::assertInstanceOf(TestController::class, $this->instance);
	}

	/**
	 * @depends testIsClassExist
	 */
	public function testTestMethod(): void
	{
		self::assertTrue( method_exists($this->instance, 'test'));
	}

}
