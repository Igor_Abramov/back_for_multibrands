<?php


namespace core;


use PHPUnit\Framework\TestCase;
use Src\core\Brand;

class BrandTest extends TestCase
{
	public function testIsClassExist(): void
	{
		$instance = new Brand();
		self::assertInstanceOf(Brand::class, $instance);
	}

}
