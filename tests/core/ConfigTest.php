<?php


namespace classes;


use PHPUnit\Framework\TestCase;
use Src\core\Config;

class ConfigTest extends TestCase
{

	public function testIsClassExist(): void
	{
		$instance = new Config();
		self::assertInstanceOf(Config::class, $instance);
	}

}
