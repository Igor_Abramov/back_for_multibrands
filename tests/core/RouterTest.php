<?php


namespace core;


use Src\core\Router;
use Symfony\Component\HttpFoundation\Request;

class RouterTest extends \PHPUnit\Framework\TestCase
{

	public function testIsClassExist(): void
	{
		$request = new Request([], [
			json_encode([
				'lpname' => 'crypto-ebook-ru',
				'firstName' => 'testU',
				'lastName' => 'reqwer',
				'phoneCountry' => '6',
				'Country' => 'RU',
				'email' => 'jalousone@mail.com',
				'phoneNumber' => '5453434',
				'phoneOperator' => '454',
				'privacyPolicy' => '1',
				'linkid' => 'qa-test-sn-crypto-ebook-ru_mm-ru'
			])
		], [], [], [], [
			'HTTP_HOST' => 'localhost',
			//'REQUEST_URI' => $item1,
		], []);

		$instance = new Router($request, __DIR__ . '/../../');
		self::assertInstanceOf(Router::class, $instance);
	}




}
