<?php


namespace services;


use PHPUnit\Framework\TestCase;
use Src\services\BookMail;

class BookMailTest extends TestCase
{
	private BookMail $instance;

	public function setUp(): void
	{
		parent::setUp();
		$this->instance = new BookMail();
	}

	public function testIsClassExist(): void
	{
		self::assertInstanceOf(BookMail::class, $this->instance);
	}

	/**
	 * @depends testIsClassExist
	 */
	public function testIsClassHasMethod__setParams(): void
	{
		self::assertTrue( method_exists($this->instance, 'setParams'));
	}

	/**
	 * @depends testIsClassExist
	 */
	public function testIsClassHasMethod__execute(): void
	{
		self::assertTrue( method_exists($this->instance, 'execute'));
	}

	/**
	 * @depends testIsClassExist
	 */
	public function testIsClassHasMethod__sendRequest(): void
	{
		self::assertTrue( method_exists($this->instance, 'sendRequest'));
	}

}
