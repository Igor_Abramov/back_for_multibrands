<?php


namespace services;


use PHPUnit\Framework\TestCase;

class GeoBan extends TestCase
{
	private \Src\services\GeoBan $instance;

	public function setUp(): void
	{
		parent::setUp();
		$this->instance = new \Src\services\GeoBan();
	}

	public function testIsClassExist(): void
	{
		self::assertInstanceOf(\Src\services\GeoBan::class, $this->instance);
	}

	/**
	 * @depends testIsClassExist
	 */
	public function testIsClassHasMethod__setParams(): void
	{
		self::assertTrue( method_exists($this->instance, 'setParams'));
	}

	/**
	 * @depends testIsClassExist
	 */
	public function testIsClassHasMethod__execute(): void
	{
		self::assertTrue( method_exists($this->instance, 'execute'));
	}

	/**
	 * @depends testIsClassExist
	 */
	public function testIsClassHasMethod__sendRequest(): void
	{
		self::assertTrue( method_exists($this->instance, 'sendRequest'));
	}
}
