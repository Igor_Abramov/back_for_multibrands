<?php


namespace services;


use Src\services\RegistrationLead;

class RegistrationLeadTest extends \PHPUnit\Framework\TestCase
{

	private \Src\services\RegistrationLead $instance;

	public function setUp(): void
	{
		parent::setUp();
		$this->instance = new RegistrationLead();
	}

	public function testIsClassExist(): void
	{
		self::assertInstanceOf(RegistrationLead::class, $this->instance);
	}

	/**
	 * @depends testIsClassExist
	 */
	public function testIsClassHasMethod__setParams(): void
	{
		self::assertTrue( method_exists($this->instance, 'setParams'));
	}

	/**
	 * @depends testIsClassExist
	 */
	public function testIsClassHasMethod__execute(): void
	{
		self::assertTrue( method_exists($this->instance, 'execute'));
	}

	/**
	 * @depends testIsClassExist
	 */
	public function testIsClassHasMethod__sendRequest(): void
	{
		self::assertTrue( method_exists($this->instance, 'sendRequest'));
	}

}
