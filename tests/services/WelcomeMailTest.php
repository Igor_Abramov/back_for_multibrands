<?php


namespace services;


use Src\services\WelcomeMail;

class WelcomeMailTest extends \PHPUnit\Framework\TestCase
{
	private WelcomeMail $instance;

	public function setUp(): void
	{
		parent::setUp();
		$this->instance = new WelcomeMail();
	}

	public function testIsClassExist(): void
	{
		self::assertInstanceOf(WelcomeMail::class, $this->instance);
	}

	/**
	 * @depends testIsClassExist
	 */
	public function testIsClassHasMethod__setParams(): void
	{
		self::assertTrue( method_exists($this->instance, 'setParams'));
	}

	/**
	 * @depends testIsClassExist
	 */
	public function testIsClassHasMethod__execute(): void
	{
		self::assertTrue( method_exists($this->instance, 'execute'));
	}

	/**
	 * @depends testIsClassExist
	 */
	public function testIsClassHasMethod__sendRequest(): void
	{
		self::assertTrue( method_exists($this->instance, 'sendRequest'));
	}
}
